LIBRARY IEEE;
USE IEEE.std_logic_1164.all;

ENTITY clk_divider_50Hz IS
  PORT (clk: IN STD_LOGIC;
        clk_50Hz: OUT STD_LOGIC);
END ENTITY;

ARCHITECTURE behavioural OF clk_divider_50Hz IS
BEGIN
  
  PROCESS (clk)
    VARIABLE count: INTEGER range 0 TO 100000000;
    VARIABLE temp: STD_LOGIC := '0';
    
  BEGIN
    IF RISING_EDGE(clk) THEN
		if(count > 499999) then
			count := 0;
      		temp := NOT temp;	
		else
			count := count + 1;
		end if;
	END IF;
	clk_50Hz <= temp;
  END PROCESS;
  
END ARCHITECTURE behavioural;
