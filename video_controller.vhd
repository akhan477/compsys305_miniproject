library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library flappy_types;
use work.flappy_types.all;

library flappy_physics;
use work.flappy_physics.all;

entity video_controller is
	port(
			clk_25mhz: in std_logic;
			color_scheme: in std_logic;
			current_game_state: in flappy_fsm_port;

			bird_x_pos: in integer range 0 to HORIZONTAL_RESOLUTION;
			bird_y_pos: in integer range 0 to VERTICAL_RESOLUTION;

			current_bullet_state: in bullet_state;	
			bullet_x: in integer range -1 to HORIZONTAL_RESOLUTION * 2 ;
			bullet_y: in integer range -1 to VERTICAL_RESOLUTION;

			pixel_row, pixel_column: in std_logic_vector(9 downto 0);
			pipe_A_x: in integer range -HORIZONTAL_RESOLUTION to HORIZONTAL_RESOLUTION*2;
			pipe_A_y: in integer range 0 to VERTICAL_RESOLUTION;
			pipe_B_x: in integer range -HORIZONTAL_RESOLUTION to HORIZONTAL_RESOLUTION*2;
			pipe_B_y: in integer range 0 to VERTICAL_RESOLUTION;
			pipe_C_x: in integer range -HORIZONTAL_RESOLUTION to HORIZONTAL_RESOLUTION*2;
			pipe_C_y: in integer range 0 to VERTICAL_RESOLUTION;
			flowers: in flower_array;
			current_score: in integer range 0 to 99;
			current_level: in integer range 1 to 3;
			game_mode_switch: in std_logic;

			red_out: out VGA_Color;
			green_out: out VGA_Color;
			blue_out: out VGA_Color
		);
end entity video_controller;

architecture derp of video_controller is

	component message_generator is
		port ( 	frame_row, frame_col, current_row, current_col : in integer range 0 to 1024;
		clk, enable : in std_logic;
		character_array : in char_array;
		font_out : out std_logic
	);
	end component message_generator;

	signal message_enable : std_logic;
	signal message_out : std_logic;
	signal pixel_x, pixel_y, message_x, message_y: integer range 0 to 1024;
	signal message_array : char_array;
	signal c_score_ones, c_score_tens, c_level : char; 

	component megaman_rom IS
		PORT
		(
			address	: 	IN STD_LOGIC_VECTOR(10 downto 0);
			clock	: 	IN STD_LOGIC ;
			q		: 	OUT STD_LOGIC_VECTOR (12 DOWNTO 0)
		);
	end component megaman_rom;
	signal megaman_rom_address: std_logic_vector(10 downto 0);
	signal megaman_rom_output : std_logic_vector(12 downto 0);

begin
		a_generator : message_generator port map (message_y, message_x, pixel_y, pixel_x, clk_25mhz, message_enable, message_array, message_out);
	megaman		: megaman_rom port map(
									   address => megaman_rom_address,
									   clock => clk_25mhz,
									   q => megaman_rom_output);

	process(clk_25mhz)
		variable red, green, blue : VGA_Color;
		variable fill, background : VGA_Color;
		variable int_score_ones, int_score_tens : integer range 0 to 9;
		variable v_score : integer range 0 to 1023;
		variable bird_row		  : integer range 1 to 63;
		variable bird_column	  : integer range 1 to 31;
	begin
		if(rising_edge(clk_25mhz)) then

			if (color_scheme = '1') then
				fill := "0000";
				background := x"F";
			else
				fill := x"F";
				background := x"0";
			end if;

			pixel_x <= to_integer(unsigned(pixel_column)); 
			pixel_y <= to_integer(unsigned(pixel_row));

			case current_game_state is
				when flappy_fsm_reset =>
					message_enable <= '0';
				when flappy_fsm_menu =>
					if ((pixel_y >= 200) AND (pixel_y <= 200 + MESSAGE_HEIGHT)) then
						message_x <= HORIZONTAL_RESOLUTION/2 - MESSAGE_WIDTH /2;
						message_y <= 200;
						message_enable <= '1';
						message_array <= (CHAR_M, CHAR_A, CHAR_I, CHAR_N, CHAR_SPACE, CHAR_M, CHAR_E, CHAR_N, CHAR_U, CHAR_SPACE);
					elsif ((pixel_y >= 400) AND (pixel_y <= 400 + MESSAGE_HEIGHT)) then
						if (game_mode_switch = '1') then 
							message_array <= (CHAR_SPACE, CHAR_SPACE, CHAR_SPACE, CHAR_G, CHAR_A, CHAR_M, CHAR_E, CHAR_SPACE, CHAR_SPACE, CHAR_SPACE );
						else 
							message_array <= (CHAR_SPACE, CHAR_T, CHAR_R, CHAR_A, CHAR_I, CHAR_N, CHAR_I, CHAR_N, CHAR_G, CHAR_SPACE);		
						end if;
						message_x <= HORIZONTAL_RESOLUTION/2 - MESSAGE_WIDTH /2;
						message_y <= 400;
						message_enable <= '1';
					else 
						message_enable <= '0';
					end if;
						
					if (message_out = '1') then
						red := fill;
						blue := fill;
						green := fill;
					else 
						red := background;
						blue := background;
						green := background;
					end if;
				when  flappy_fsm_play | flappy_fsm_dead | flappy_fsm_training => --flappy_fsm_idle

					if( current_bullet_state = bullet_state_flying) and ((pixel_x > bullet_x) and (pixel_x < bullet_x + bullet_size)) and ((pixel_y > bullet_y) and (pixel_y < bullet_y + bullet_size)) then
						red := x"F";
						blue := x"0";
						green := x"0";
					-- Pixel is a pipe
					elsif (point_is_pipe(pixel_x, pixel_y, pipe_A_x, pipe_A_y)
					OR (point_is_pipe(pixel_x, pixel_y, pipe_B_x, pipe_B_y))
					OR (point_is_pipe(pixel_x, pixel_y, pipe_C_x, pipe_C_y))) then

						red := x"0";
						blue := x"0";
						green := x"F";

					else
						red := background;
						blue := background;
						green := background;
					end if;

					if(flowers(0) = '1') then
						if(pixel_x >= Pipe_A_x + 5 and pixel_x <= Pipe_A_x + 45) and (pixel_y >= Pipe_A_y +30 and pixel_y <= Pipe_A_y + 120) then
							red := COLOR_RED.red;
							blue := COLOR_RED.blue;
							green := COLOR_RED.GREEN;
						end if;
					end if;

					if(flowers(1) = '1') then
						if(pixel_x >= Pipe_B_x + 5 and pixel_x <= Pipe_B_x + 45) and (pixel_y >= Pipe_B_y +30 and pixel_y <= Pipe_B_y + 120) then
							red := COLOR_RED.red;
							blue := COLOR_RED.blue;
							green := COLOR_RED.GREEN;
						end if;
					end if;

					if (flowers(2) = '1') then
						if(pixel_x >= Pipe_C_x + 5 and pixel_x <= Pipe_C_x + 45) and (pixel_y >= Pipe_C_y +30 and pixel_y <= Pipe_C_y + 120) then
							red := COLOR_RED.red;
							blue := COLOR_RED.blue;
							green := COLOR_RED.GREEN;
						end if;
					end if;

					if( ((pixel_x > bird_x_pos) and (pixel_x < bird_x_pos + BIRD_HORIZONTAL_SIZE)) 				--pixel is bird
					and ((pixel_y > bird_y_pos) and (pixel_y < bird_y_pos + BIRD_VERTICAL_SIZE)) ) then
						--red := fill;
						--blue := fill;
						--green := fill;
						bird_row := pixel_y - bird_y_pos;
						bird_column := pixel_x - bird_x_pos;
						megaman_rom_address <= std_logic_vector(to_unsigned(bird_row * 31 + bird_column, megaman_rom_address'length));

						if(megaman_rom_output(0) = '1') then -- transparent pixel
							red := megaman_rom_output(12 downto 9);
							green := megaman_rom_output(8 downto 5);
							blue := megaman_rom_output(4 downto 1);
						end if;
					end if;
					
					--level message
					case current_level is
					when 1 =>
						c_level <= CHAR_1;
					when 2 =>
						c_level <= CHAR_2;
					when 3 =>
						c_level <= CHAR_3;
					when others =>
						c_level <= CHAR_SPACE;
					end case;
					
					-- score message
					v_score := current_score;
					int_score_ones := v_score rem 10;
					v_score := v_score / 10;
					int_score_tens := v_score rem 10;
					
					case int_score_ones is
						when 0 =>
							c_score_ones <= CHAR_0;
						when 1 =>
							c_score_ones <= CHAR_1;
						when 2 =>
							c_score_ones <= CHAR_2;
						when 3 =>
							c_score_ones <= CHAR_3;
						when 4 =>
							c_score_ones <= CHAR_4;
						when 5 =>
							c_score_ones <= CHAR_5;
						when 6 =>
							c_score_ones <= CHAR_6;
						when 7 =>
							c_score_ones <= CHAR_7;
						when 8 =>
							c_score_ones <= CHAR_8;
						when 9 =>
							c_score_ones <= CHAR_9;
					end case;
					
					case int_score_tens is
						when 0 =>
							c_score_tens <= CHAR_0;
						when 1 =>
							c_score_tens <= CHAR_1;
						when 2 =>
							c_score_tens <= CHAR_2;
						when 3 =>
							c_score_tens <= CHAR_3;
						when 4 =>
							c_score_tens <= CHAR_4;
						when 5 =>
							c_score_tens <= CHAR_5;
						when 6 =>
							c_score_tens <= CHAR_6;
						when 7 =>
							c_score_tens <= CHAR_7;
						when 8 =>
							c_score_tens <= CHAR_8;
						when 9 =>
							c_score_tens <= CHAR_9;
					end case;
						
					--choose message
					if ((pixel_x >= 10) AND (pixel_x <= 10 + MESSAGE_WIDTH)) then
						if current_level = 0 then
							message_array <= (CHAR_T, CHAR_R, CHAR_A, CHAR_I, CHAR_N, CHAR_I, CHAR_N, CHAR_G, CHAR_SPACE, CHAR_SPACE);
						else	
							message_array <= (CHAR_L, CHAR_E, CHAR_V, CHAR_E, CHAR_L, CHAR_SPACE, c_level, CHAR_SPACE, CHAR_SPACE, CHAR_SPACE);
						end if;
						message_x <= 10;
						message_y <= 10;
						message_enable <= '1';
					elsif ((pixel_x >= 540) AND (pixel_x <= 540 + MESSAGE_WIDTH)) then
						message_array <= (CHAR_S, CHAR_C, CHAR_O, CHAR_R, CHAR_E, CHAR_SPACE, c_score_tens, c_score_ones, CHAR_SPACE, CHAR_SPACE);
						message_x <= 540;
						message_y <= 10;
						message_enable <= '1';
					else
						message_enable <= '0';
					end if;						
					
					if (message_out = '1') then
							red := fill;
							blue := fill;
							green := fill;
						end if;			
						
				when others =>
			end case;

		end if;
		red_out <= red;
		green_out <= green;
		blue_out <= blue;
	end process;
end architecture derp;
