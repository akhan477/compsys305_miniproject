M = 0;
N = 1;
Depth  = 0;
Width  = 12;
AR = 'DEC';
DR = 'BIN';
fileName = 'image2.txt';
D = dir('*.jpg');
fid = fopen(fileName,'W');
if fid ~= -1
    fprintf(fid,'DEPTH = ;\r\nWIDTH  = %d;\r\nADDRESS_RADIX = %s;\r\nDATA_RADIX = %s;\r\nCONTENT\r\nBEGIN\r\n',Width,AR,DR);      
    for l = 1:numel(D)
        Image = imread(D(l).name);
        [X,Y,Z] = size(Image);
        A = X;
        X = Y;
        Y = A;
        Xb = dec2bin(X,12);
        Yb = dec2bin(Y,12);
        fprintf(fid,'%d\t:\t%s;\r\n%d\t:\t%s;\r\n',M,Xb,M+1,Yb);
        M = M+2;
        Depth = Depth + X*Y + 2;
        for i = 1:Y
            for j = 1:X
                OutColour = '';
                green = 0;
                for k = 1:3 
                    color = Image(i,j,k);
                    color = idivide(color, 16, 'round');
                    if color == 16
                        color = 15;
                    end
                    color = dec2bin(color,4);
                    OutColour = strcat(OutColour,color);   
                end
                fprintf(fid,'%d\t:\t%s;\t\r\n' ,M ,OutColour);
                M = M+1;
            end
        end
        fprintf(fid,'%d\t:\t111111111111;\r\n',M); % end of image
    end
    X = 0;
    Depth = Depth + 1;
    while Depth > 2^X
        X = X+1;        
    end
    
    fprintf(fid,'END;\r\n%d', 2^X);
    fclose(fid);
end