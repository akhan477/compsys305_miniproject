clear all;
clc;

%Converts a PNG file to an Alteral MIF file
%Takes in an arbitrary large image
%Takes in a file called derp.png and outputs derp.mif
%The result is: (red(4 bits), green(4 bits), blue(4bits) + transparency
%bit);
%Written by Jake Price(FrozenXZeus)
%Using code provided by Segar Manoharan and Andrew Liese
%Some rights reserved
%...derp


% Open file
str_InputFileName = 'derp.png';
str_OutputFileName = 'derp.mif';

file_outputFile = fopen(str_OutputFileName, 'W');

if (file_outputFile ~= 1) % File opened successfully
    [img_pixelArray, map, imgAlpha] = imread(str_InputFileName);
	[row, column, colors] = size(img_pixelArray);
    
    numberOfPixels = row * column;
    depth = 2^(ceil(log2(numberOfPixels)));
    numberOfDigits = floor(log10(depth))+1;
    outputString = strcat('%0',num2str(numberOfDigits),'d\t:%s;\r\n');
    
    %output boring headers, change if needed
	fprintf(file_outputFile, 'DEPTH = %d;\r\n', depth);
	fprintf(file_outputFile, 'WIDTH = 13;\r\n');
	fprintf(file_outputFile, 'ADDRESS_RADIX=DEC;\r\n');
	fprintf(file_outputFile, 'DATA_RADIX=BIN;\r\n');
	fprintf(file_outputFile, 'CONTENT\r\n');
	fprintf(file_outputFile, 'BEGIN\r\n');

	currentDigit = 0;
	for i = 1:row
		for j = 1:column

			current_red = img_pixelArray(i, j, 1);
			current_green = img_pixelArray(i, j, 2);
			current_blue = img_pixelArray(i, j, 3);

			stringToWrite = strcat(dec2bin(idivide(current_red, 16), 4), dec2bin(idivide(current_green, 16), 4), dec2bin(idivide(current_blue, 16), 4));

            %Concatenate the transparency bit
			if imgAlpha(i,j) < 10
				stringToWrite = strcat(stringToWrite, '0');
			else
				stringToWrite = strcat(stringToWrite, '1');
            end

            %Burn to file
			fprintf(file_outputFile,outputString, currentDigit, stringToWrite);
			currentDigit = currentDigit + 1;
		end
	end

	fprintf(file_outputFile, 'END;');
    fclose(file_outputFile);
end
