library ieee;
use ieee.std_logic_1164.all;

package flappy_types is

	-- Setting constants
	constant HORIZONTAL_RESOLUTION: integer range 0 to 1023;
	constant VERTICAL_RESOLUTION: integer range 0 to 1023;
	--Game Constants
	constant BIRD_VERTICAL_SIZE: integer range 0 to 63;
	constant BIRD_HORIZONTAL_SIZE: integer range 0 to 63;
	constant PIPE_OPENING_SIZE: integer range 0 to 255;
	constant PIPE_WIDTH: integer range 0 to 63;
	constant PIPE_SPEED_NORMAL: integer range 0 to 9;
	constant PIPE_SPEED_HARD: integer range 0 to 9;
	constant LEVEL_2_THRESHHOLD: integer range 0 to 1024; 
	constant LEVEL_3_THRESHHOLD: integer range 0 to  1024;

	subtype flower_array is std_logic_vector(2 downto 0);
	constant flower_width: integer range 0 to 127;
	constant flower_height: integer range 0 to 127;
	--Type declarations
	subtype VGA_Color is std_logic_vector(3 downto 0);

	type flappy_fsm is (reset, menu, play, dead, training); --idle? 
	subtype flappy_fsm_port is std_logic_vector(2 downto 0);
	constant flappy_fsm_reset: flappy_fsm_port;
	constant flappy_fsm_menu: flappy_fsm_port;
	constant flappy_fsm_idle: flappy_fsm_port;
	constant flappy_fsm_play: flappy_fsm_port;
	constant flappy_fsm_dead: flappy_fsm_port;
	constant flappy_fsm_training: flappy_fsm_port;
	
	--message dimensions
	constant MESSAGE_HEIGHT : integer range 0 to 8;
	constant CHARACTER_WIDTH : integer range 0 to 8;
	constant MESSAGE_WIDTH : integer range 0 to 80;
	subtype char is std_logic_vector(5 downto 0);
	type char_array is array (0 to 9) of char;
	
	--characters
	constant CHAR_A : char := O"01";
	constant CHAR_B : char := O"02";
	constant CHAR_C : char := O"03";
	constant CHAR_D : char := O"04";
	constant CHAR_E : char := O"05";
	constant CHAR_F : char := O"06";
	constant CHAR_G : char := O"07";
	constant CHAR_H : char := O"10";
	constant CHAR_I : char := O"11";
	constant CHAR_J : char := O"12";
	constant CHAR_K : char := O"13";
	constant CHAR_L : char := O"14";
	constant CHAR_M : char := O"15";
	constant CHAR_N : char := O"16";
	constant CHAR_O : char := O"17";
	constant CHAR_P : char := O"20";
	constant CHAR_Q : char := O"21";
	constant CHAR_R : char := O"22";
	constant CHAR_S : char := O"23";
	constant CHAR_T : char := O"24";
	constant CHAR_U : char := O"25";
	constant CHAR_V : char := O"26";
	constant CHAR_W : char := O"27";
	constant CHAR_X : char := O"30";
	constant CHAR_Y : char := O"31";
	constant CHAR_Z : char := O"32";
	
	constant CHAR_0 : char := O"60";
	constant CHAR_1 : char := O"61";
	constant CHAR_2 : char := O"62";
	constant CHAR_3 : char := O"63";
	constant CHAR_4 : char := O"64";
	constant CHAR_5 : char := O"65";
	constant CHAR_6 : char := O"66";
	constant CHAR_7 : char := O"67";
	constant CHAR_8 : char := O"70";
	constant CHAR_9 : char := O"71";
	
	constant CHAR_SPACE : char := O"40";
	
	--bullets
	subtype bullet_state is std_logic;
	constant bullet_state_idle: bullet_state;
	constant bullet_state_flying: bullet_state;
	constant bullet_size: integer range 0 to 7;
	constant bullet_speed: integer range 0 to 7;

	
	type pixel is
		record
			red: VGA_Color;
			green: VGA_Color;
			blue: VGA_Color;
		end record;
	-- Pixel Constants
	constant COLOR_RED, COLOR_BLUE, COLOR_GREEN, COLOR_BLACK, COLOR_WHITE: pixel;
	type point is
		record
			x: integer range -HORIZONTAL_RESOLUTION to HORIZONTAL_RESOLUTION*2;
			y: integer range -VERTICAL_RESOLUTION to VERTICAL_RESOLUTION*2;
		end record;

end package flappy_types;

package body flappy_types is
	--Pixel definitions
	constant COLOR_RED	: pixel:= (red=>x"F", blue=>x"0", green=>x"0");
	constant COLOR_BLUE	: pixel:= (red=>x"0", blue=>x"F", green=>x"0");
	constant COLOR_GREEN: pixel:= (red=>x"0", blue=>x"0", green=>x"F");
	constant COLOR_BLACK: pixel:= (red=>x"0", blue=>x"0", green=>x"0");
	constant COLOR_WHITE: pixel:= (red=>x"F", blue=>x"F", green=>x"F");
	--setting constants
	constant HORIZONTAL_RESOLUTION: integer := 639;
	constant VERTICAL_RESOLUTION: integer := 479;
	--Game constants
	constant flappy_fsm_reset: flappy_fsm_port	:= "000";
	constant flappy_fsm_menu: flappy_fsm_port 	:= "001";
	constant flappy_fsm_idle: flappy_fsm_port 	:= "010";
	constant flappy_fsm_play: flappy_fsm_port	:= "011";
	constant flappy_fsm_dead: flappy_fsm_port	:= "100";
	constant flappy_fsm_training: flappy_fsm_port := "101";

	constant bullet_state_idle: bullet_state := '0';
	constant bullet_state_flying: bullet_state := '1';
	constant bullet_size: integer range 0 to 7 := 6;
	constant bullet_speed: integer range 0 to 7 := 6;

	constant BIRD_VERTICAL_SIZE: integer := 38;
	constant BIRD_HORIZONTAL_SIZE: integer:= 31;
	constant PIPE_OPENING_SIZE: integer range 0 to 255 := 150;
	constant PIPE_WIDTH: integer range 0 to 63:= 50;

	constant flower_width: integer range 0 to 127 := 40;
	constant flower_height: integer range 0 to 127 := 90;
	
	--Difficulties
	constant LEVEL_2_THRESHHOLD: integer range 0 to 1023 := 2; --1024, 5
	constant LEVEL_3_THRESHHOLD: integer range 0 to 1023 := 4;--1024, 50;

	constant PIPE_SPEED_NORMAL: integer range 0 to 9:= 4;
	constant PIPE_SPEED_HARD: integer range 0 to 9:= 9;
	
	--message dimensionse
	constant MESSAGE_HEIGHT : integer range 0 to 8 := 8;
	constant CHARACTER_WIDTH : integer range 0 to 8 := 8;
	constant MESSAGE_WIDTH : integer range 0 to 80 := 80;

end flappy_types;
