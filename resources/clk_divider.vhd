library ieee;
use ieee.std_logic_1164.all;

entity clk_divider is
	generic( N: integer := 2);
	port (clk: std_logic;
		clk_out: std_logic);
end entity clk_divider;

architecture derp of clk_divider is
begin
	process(clk)
		variable a: integer range 0 to N+1 := 0;
	begin
		if(rising_edge(clk)) then
			if(a = N) then 
				clk_out <= '1';
				a <= 0;
			else
				clk_out <= '0';
				a <= a + 1;
			end if;
		end if;
	end process;

end architecture derp;
