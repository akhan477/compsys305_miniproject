library ieee;
use ieee.std_logic_1164.all;

library work;
use work.flappy_types.all;

package flappy_physics is
	function bird_collides_with_screen(bird_y : integer) return boolean;
	function item_collides_with_pipe(pipes_x: integer;
   									  pipes_y: integer;
								  	  bird_x, bird_y, horizontal_size, vertical_size: integer) return boolean;
	function point_is_pipe(pixel_x, pixel_y, pipe_x, pipe_y: integer) return boolean;									
end package flappy_physics;

package body flappy_physics is
	function bird_collides_with_screen(bird_y : integer) return boolean is

	begin
		return (bird_y = 0) or (bird_y + BIRD_VERTICAL_SIZE > VERTICAL_RESOLUTION);
	end bird_collides_with_screen;

	function item_collides_with_pipe(pipes_x: integer;
   									  pipes_y: integer;
								  	  bird_x, bird_y, horizontal_size, vertical_size: integer) return boolean is
	begin
		return ( ( (pipes_x < bird_x + horizontal_size) and (pipes_x+PIPE_WIDTH > bird_x) ) and (pipes_y > bird_y or pipes_y + PIPE_OPENING_SIZE < bird_y + vertical_size ));
	end item_collides_with_pipe;

	function point_is_pipe(pixel_x, pixel_y, pipe_x, pipe_y: integer) return boolean is
	begin
		return ((pixel_x > pipe_x) AND (pixel_x < pipe_x + PIPE_WIDTH) AND 
		((pixel_y < pipe_y) OR (pixel_y > pipe_y + PIPE_OPENING_SIZE)));
	end point_is_pipe;
end flappy_physics; 
