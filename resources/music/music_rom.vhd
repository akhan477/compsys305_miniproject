LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_UNSIGNED.all;

LIBRARY altera_mf;
USE altera_mf.all;

ENTITY music_rom IS
	generic(
	file_name: STRING:= "derp.mif";
	number_of_notes: integer := 1024;
	bus_width: integer := 11);
	PORT
	(
		address	: 	IN STD_LOGIC_VECTOR(bus_width - 1 downto 0);
		clock	: 	IN STD_LOGIC ;
		q		: 	OUT STD_LOGIC_VECTOR (9 DOWNTO 0)
	);
END music_rom;


ARCHITECTURE derp OF music_rom IS

	SIGNAL rom_data		: STD_LOGIC_VECTOR (9 DOWNTO 0);
	SIGNAL rom_address	: STD_LOGIC_VECTOR (bus_width - 1 DOWNTO 0);

	COMPONENT altsyncram
		GENERIC (
		address_aclr_a			: STRING;
		clock_enable_input_a	: STRING;
		clock_enable_output_a	: STRING;
		init_file				: STRING;
		intended_device_family	: STRING;
		lpm_hint				: STRING;
		lpm_type				: STRING;
		numwords_a				: NATURAL;
		operation_mode			: STRING;
		outdata_aclr_a			: STRING;
		outdata_reg_a			: STRING;
		widthad_a				: NATURAL;
		width_a					: NATURAL;
		width_byteena_a			: NATURAL
	);
	PORT (
			 clock0		: IN STD_LOGIC ;
			 address_a	: IN STD_LOGIC_VECTOR (bus_width - 1 DOWNTO 0);
			 q_a			: OUT STD_LOGIC_VECTOR (9 DOWNTO 0)
		 );
	END COMPONENT;

BEGIN
	--rom_mux_output	<= sub_wire0(7 DOWNTO 0);

	altsyncram_component : altsyncram
	GENERIC MAP (
					address_aclr_a => "NONE",
					clock_enable_input_a => "BYPASS",
					clock_enable_output_a => "BYPASS",
					init_file => file_name,
					intended_device_family => "Cyclone III",
					lpm_hint => "ENABLE_RUNTIME_MOD=NO",
					lpm_type => "altsyncram",
					operation_mode => "ROM",
					outdata_aclr_a => "NONE",
					outdata_reg_a => "UNREGISTERED",
					numwords_a => number_of_notes,
					widthad_a => bus_width,
					width_a => 10,
					width_byteena_a => 1
				)
	PORT MAP (
				 clock0 => clock,
				 address_a => rom_address,
				 q_a => rom_data
			 );

	rom_address <= address;
	q <= rom_data;

END derp;
