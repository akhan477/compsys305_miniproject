library ieee;
use ieee.std_logic_1164.all;

entity buzzer_out is
	port(
	clk: in std_logic;
	clk_50hz: in std_logic;
	enable: in std_logic;
	note_in: in integer range 0 to 1250000;
	buzzer_out:out std_logic
	);
end entity buzzer_out;

architecture derp of buzzer_out is
	signal current_note: integer range 0 to 1250000;
begin
	load_registers: process(clk_50hz)
	begin
		if(rising_edge(clk_50hz)) then
			current_note <= note_in;
		end if;
	end process;

	output: process(clk, enable)
		variable clk_div: integer range 0 to 1250000:=0;
		variable clk_out: std_logic := '0';
	begin
		if(rising_edge(clk)) then
			clk_div := clk_div + 1;
			if (clk_div > current_note/2) then
				clk_div := 0;
				clk_out := NOT clk_out;
			end if;
		end if;
		buzzer_out <= clk_out and enable;
	end process;

end derp;
