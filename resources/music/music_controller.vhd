library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.flappy_types.all;

entity music_controller is
	port(
			clk_50hz: in std_logic;
			game_state: in flappy_fsm_port;
			menu_freq_in: in std_logic_vector(9 downto 0);
			game_freq_in: in std_logic_vector(9 downto 0);
			music_out: out std_logic;
			note_out: out integer range 0 to 1250000;
			menu_address: out std_logic_vector(10 downto 0);
			game_address: out std_logic_vector(12 downto 0));
end entity music_controller;

architecture derp of music_controller is
	signal last_fsm_value: flappy_fsm_port;
begin
	process(clk_50hz)
		variable current_note_index: integer range 0 to 16000 := 0;
		variable current_note: integer range 0 to 1023;
	begin
		if(rising_edge(clk_50hz)) then
			last_fsm_value <= game_state;
			case game_state is
				when flappy_fsm_reset=>
					current_note_index := 0;
					music_out <= '0';
				when flappy_fsm_menu|flappy_fsm_play| flappy_fsm_training | flappy_fsm_dead=>
					if(last_fsm_value = flappy_fsm_menu and game_state = flappy_fsm_play) then
						current_note_index := 0;
					end if;
					
					current_note_index := current_note_index + 1;
					
					if (game_state = flappy_fsm_menu) then
						if(current_note_index > 1997) then
							current_note_index := 0;
						end if;
						menu_address <= std_logic_vector(to_unsigned(current_note_index, menu_address'length));
						current_note := TO_INTEGER(UNSIGNED(menu_freq_in));
					else
						if(current_note_index > 5501) then
							current_note_index := 0;
						end if;
						game_address <= std_logic_vector(to_unsigned(current_note_index, game_address'length));
						current_note := TO_INTEGER(UNSIGNED(game_freq_in));
					end if;

					if(current_note = 0) then
						music_out <= '0';
					else
						note_out <= 12500000 / current_note;
						music_out<= '1';
					end if;
				when others =>
					music_out <= '0';
			end case;
		end if;
	end process;

end derp;
