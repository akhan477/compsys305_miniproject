LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_UNSIGNED.all;

LIBRARY altera_mf;
USE altera_mf.all;

ENTITY megaman_rom_test IS
	PORT
	(
		address	: 	IN STD_LOGIC_VECTOR(10 downto 0);
		clock	: 	IN STD_LOGIC ;
		q		: 	OUT STD_LOGIC
	);
END megaman_rom_test;


ARCHITECTURE derp OF megaman_rom_test IS

	SIGNAL rom_data		: STD_LOGIC;
	SIGNAL rom_address	: STD_LOGIC_VECTOR (10 DOWNTO 0);

	COMPONENT altsyncram
		GENERIC (
		address_aclr_a			: STRING;
		clock_enable_input_a	: STRING;
		clock_enable_output_a	: STRING;
		init_file				: STRING;
		intended_device_family	: STRING;
		lpm_hint				: STRING;
		lpm_type				: STRING;
		numwords_a				: NATURAL;
		operation_mode			: STRING;
		outdata_aclr_a			: STRING;
		outdata_reg_a			: STRING;
		widthad_a				: NATURAL;
		width_a					: NATURAL;
		width_byteena_a			: NATURAL
	);
	PORT (
			 clock0		: IN STD_LOGIC ;
			 address_a	: IN STD_LOGIC_VECTOR (10 DOWNTO 0);
			 q_a		: OUT STD_LOGIC
		 );
	END COMPONENT;

BEGIN
	--rom_mux_output	<= sub_wire0(7 DOWNTO 0);

	altsyncram_component : altsyncram
	GENERIC MAP (
					address_aclr_a => "NONE",
					clock_enable_input_a => "BYPASS",
					clock_enable_output_a => "BYPASS",
					init_file => "megaman_rom_test.mif",
					intended_device_family => "Cyclone III",
					lpm_hint => "ENABLE_RUNTIME_MOD=NO",
					lpm_type => "altsyncram",
					numwords_a => 1178,
					operation_mode => "ROM",
					outdata_aclr_a => "NONE",
					outdata_reg_a => "UNREGISTERED",
					widthad_a => 11,
					width_a => 1,
					width_byteena_a => 1
				)
	PORT MAP (
				 clock0 => clock,
				 address_a => rom_address,
				 q_a => rom_data
			 );

	rom_address <= address;
	q <= rom_data;

END derp;

