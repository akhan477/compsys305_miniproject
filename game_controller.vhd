library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library flappy_types;
use work.flappy_types.all;

library flappy_physics;
use work.flappy_physics.all;

entity game_controller is
	port(
			--Clock should be 50Hz
			clk_50Hz: in std_logic;	
			flap: in std_logic;
			shoot: in std_logic;
			generated_pipe_location: in std_logic_vector(8 downto 0);
			game_mode_switch : in std_logic;
			random_reset: out std_logic;

			--Game Values
			game_state: out flappy_fsm_port;
			bird_x_pos: out integer range 0 to HORIZONTAL_RESOLUTION;
			bird_y_pos: out integer range 0 to VERTICAL_RESOLUTION + 30;

			--Bullets
			current_bullet_state: out bullet_state;	
			bullet_x: out integer range -1 to HORIZONTAL_RESOLUTION * 2;
			bullet_y: out integer range -1 to VERTICAL_RESOLUTION;

			--Pipes
			pipe_A_x: out integer range -HORIZONTAL_RESOLUTION to HORIZONTAL_RESOLUTION*2;
			pipe_A_y: out integer range 0 to VERTICAL_RESOLUTION;
			pipe_B_x: out integer range -HORIZONTAL_RESOLUTION to HORIZONTAL_RESOLUTION*2;
			pipe_B_y: out integer range 0 to VERTICAL_RESOLUTION;
			pipe_C_x: out integer range -HORIZONTAL_RESOLUTION to HORIZONTAL_RESOLUTION*2;
			pipe_C_y: out integer range 0 to VERTICAL_RESOLUTION;
			flowers : out flower_array;
			score: out integer range 0 to  99;
			level : out integer range 0 to 3
		);
end entity game_controller;

architecture pipes of game_controller is

	signal current_playing_state : flappy_fsm := reset;
	signal current_level : integer range 0 to 3;
	--signal current_game_state: game_state;
	--Constant declarations
	constant GRAVITY: integer range 0 to 1 := 1;
	constant flap_speed: integer range -16 to 15 := -12; -- -15
	constant MAX_FALLING_SPEED: integer range -32 to 31 := 14; --15
begin
	world_step: process(clk_50Hz)
		type pipe_array_x is array (0 to 2) of integer range -HORIZONTAL_RESOLUTION to HORIZONTAL_RESOLUTION*2;
		type pipe_array_y is array (0 to 2) of integer range 0 to VERTICAL_RESOLUTION;
		type game_level is (level1, level2, level3);
		--NOTE: A positive velocity means the bird is going DOWN
		--NOTE: velocity is SIGNED
		variable velocity: integer range -32 to 31;
		--The point represents the top left corner of the bird
		variable v_bird_x: integer range 0 to HORIZONTAL_RESOLUTION ;
		variable v_bird_y: integer range 0 to VERTICAL_RESOLUTION + 30;

		variable v_bullet_state: bullet_state;
		variable v_bullet_x: integer range -1 to HORIZONTAL_RESOLUTION *2 ;
		variable v_bullet_y: integer range -1 to VERTICAL_RESOLUTION ;
		--Pipes store the location of the top opening of the pipe
		variable pipes_x: pipe_array_x;
		variable pipes_y: pipe_array_y;
		variable current_score: integer range 0 to 99;
		variable v_flowers: flower_array;

		variable current_pipe_speed : integer range 0 to 9;

		variable pipe_reset_counter: integer range 0 to 3 := 0;

		variable death_counter: integer range 0 to 255;
		variable death_standstill: bit;

	begin
		if(rising_edge(clk_50Hz)) then 
			case current_playing_state is
				-------------------------------------------------------------------------
				--						Reset State									   --
				-------------------------------------------------------------------------
				when reset=>
					v_bird_x := 150;
					v_bird_y := 240;
					velocity := flap_speed;
					current_score := 0;
					current_level <= 1;
					
					--Initialize the first 3 pipes to random
					pipes_x := (400 ,800, 1200);
					if(pipe_reset_counter < pipes_y'length) then
						pipes_y(pipe_reset_counter) := TO_INTEGER(UNSIGNED(generated_pipe_location)); 
						pipe_reset_counter := pipe_reset_counter + 1;
					else
						pipe_reset_counter := 0;
						current_playing_state <= menu;
					end if;


					v_bullet_state := bullet_state_idle;
					v_bullet_x := -1;
					v_bullet_y := -1;

					v_flowers:= "000";

					death_counter := 0;
					death_standstill := '1';

				--Next level logic

				-------------------------------------------------------------------------
				--						Menu State									   --
				-------------------------------------------------------------------------
				when menu=>
					if(flap = '1') then
						if (game_mode_switch = '1') then 
							current_playing_state <= play;
						else
							current_playing_state <= training;
						end if;
					end if;


				-------------------------------------------------------------------------
				--						Idle State									   --
				-------------------------------------------------------------------------

				-- TODO: decide if this is wanted or not
				--when idle =>
				--	if(flap = '1') then
				--		current_playing_state <= play;
				--	end if;
				
				-------------------------------------------------------------------------
				--						Play and Training States					   --
				-------------------------------------------------------------------------
				
				when play | training =>
					--bird jumps on click
					if(flap = '1') then
						velocity := flap_speed;
					else
						velocity := velocity + GRAVITY;
						if(velocity > MAX_FALLING_SPEED) then
							velocity := MAX_FALLING_SPEED;
						end if;
					end if;
					v_bird_y := v_bird_y + velocity;
	
				--Move pipes
				for i in pipes_x'range loop
					pipes_x(i) := pipes_x(i) - current_pipe_speed;
					
					--if pipe is off screen
					--move pipe to other side
					if(pipes_x(i) + PIPE_WIDTH < 0) then
						pipes_x(i) := pipes_x( (i+2) mod pipes_x'length) + 400 ;
						pipes_y(i) := TO_INTEGER(UNSIGNED(generated_pipe_location));
--						if(current_level = 3) then
--							v_flowers(i) := '1';
--						end if;
					end if;
					
					
					-- check if the bird is hitting a pipe
					if item_collides_with_pipe(pipes_x(i), pipes_y(i), v_bird_x, v_bird_y, BIRD_HORIZONTAL_SIZE, BIRD_VERTICAL_SIZE) then
						--well shet
						current_playing_state <= dead;
					end if;

					--Score is incremented if the bird passes the back of a pipe
					if ((v_bird_x >= pipes_x(i)) AND (v_bird_x < (pipes_x(i) + current_pipe_speed))) then
						current_score := current_score + 1;
					end if;		

					--Perform top and bottom bounds checking
					--Check for collisions
					if ( bird_collides_with_screen(v_bird_y) ) then
					----Check for top and bottom collisions
						current_playing_state <= dead;
					end if;
					
				--  Pipe speed is constant in training mode								
				
				if (current_playing_state = training) then
					current_pipe_speed := PIPE_SPEED_NORMAL;
					current_level <= 0;
				else	--Things that only exist in GAME mode
				 
				-- Change pipe speed based on current level
					case current_level is
						when 1 =>
							current_pipe_speed := PIPE_SPEED_NORMAL;
						when 2 | 3 =>
							current_pipe_speed := PIPE_SPEED_HARD;
						when others =>
							 current_pipe_speed := PIPE_SPEED_NORMAL;								
					end case;
					
					--Launch bullet if needed
					if(shoot = '1') then
						if(v_bullet_state = bullet_state_idle) then
							v_bullet_state := bullet_state_flying;
							v_bullet_x := v_bird_x + BIRD_HORIZONTAL_SIZE;
							v_bullet_y := v_bird_y + (BIRD_VERTICAL_SIZE/2 - bullet_size/2);
						end if;
					end if;

					--Move bullets
					if(v_bullet_state = bullet_state_flying) then
						v_bullet_x := v_bullet_x + bullet_speed;
					end if;					
					
					
					--Check for bullets
						if item_collides_with_pipe(pipes_x(i), pipes_y(i), v_bullet_x, v_bullet_y, bullet_size, bullet_size) or v_bullet_x + bullet_size >= HORIZONTAL_RESOLUTION then
							v_bullet_state := bullet_state_idle;
							v_bullet_x := -1;
							v_bullet_y := -1;
						end if;
						
					if ((pipes_x(i) > HORIZONTAL_RESOLUTION) and (current_level = 3)) then
						v_flowers(i) := '1';
					end if;
					
					if v_flowers(i) = '1' then
							--If Bullet collides with flower
							if ( (pipes_x(i) + 5 < v_bullet_x + bullet_size) and (pipes_x(i) + 45 > v_bullet_x) and (pipes_y(i) + 30 < v_bullet_y + bullet_size) and (pipes_y(i) + 120 > v_bullet_y) ) then
								v_bullet_state := bullet_state_idle;
								v_bullet_x := -1;
								v_bullet_y := -1;
								v_flowers(i) := '0';
							end if;
													
						--If bird collides with flower
						if ( (pipes_x(i) + 5 < v_bird_x + BIRD_HORIZONTAL_SIZE) and (pipes_x(i) + 45 > v_bird_x) and (pipes_y(i) + 30 < v_bird_y + BIRD_VERTICAL_SIZE) and (pipes_y(i) + 120 > v_bird_y) ) then
							current_playing_state <= dead;
						end if;
					end if;
					
					--Next level logic
						case current_score is
							when 0 to LEVEL_2_THRESHHOLD - 1 =>
								current_level <= 1;
							when LEVEL_2_THRESHHOLD to LEVEL_3_THRESHHOLD - 1 =>
								current_level <= 2;
							when LEVEL_3_THRESHHOLD to current_score'high =>
								current_level <= 3;
						end case;
					end if;		
					level <= current_level;	
				end loop;
									
				
				-------------------------------------------------------------------------
				--						Play State									   --
				-------------------------------------------------------------------------
--				when play =>
--					--Move Bird
--					if(flap = '1') then
--						velocity := flap_speed;
--					else
--						velocity := velocity + GRAVITY;
--						if(velocity > MAX_FALLING_SPEED) then
--							velocity := MAX_FALLING_SPEED;
--						end if;
--					end if;
--					v_bird_y := v_bird_y + velocity;
--
--					--Launch bullet if needed
--					if(shoot = '1') then
--						if(v_bullet_state = bullet_state_idle) then
--							v_bullet_state := bullet_state_flying;
--							v_bullet_x := v_bird_x + BIRD_HORIZONTAL_SIZE;
--							v_bullet_y := v_bird_y + (BIRD_VERTICAL_SIZE/2 - bullet_size/2);
--						end if;
--					end if;
--
--					--Move bullets
--					if(v_bullet_state = bullet_state_flying) then
--						v_bullet_x := v_bullet_x + bullet_speed;
--					end if;
--
--					if(v_bird_y < 0) then
--						v_bird_y := 0;
--					end if;

--					-- Change pipe speed based on current level
--					case current_level is
--						when level1 =>
--							current_pipe_speed := PIPE_SPEED_NORMAL;
--						when others =>
--							current_pipe_speed := PIPE_SPEED_HARD;								
--					end case;
--
--					for i in pipes_x'range loop
--						--Move pipes
--						pipes_x(i) := pipes_x(i) - current_pipe_speed;
--
--						--if pipe is off screen
--						--move pipe to other side
--						if(pipes_x(i) + PIPE_WIDTH < 0) then
--							pipes_x(i) := pipes_x( (i+2) mod pipes_x'length) + 400 ;
--							pipes_y(i) := TO_INTEGER(UNSIGNED(generated_pipe_location));
--							if(current_level = level3) then
--								v_flowers(i) := '1';
--							end if;
--						end if;
--
--						--Add score
--						if ((v_bird_x >= pipes_x(i)) AND (v_bird_x < (pipes_x(i) + current_pipe_speed))) then
--							current_score := current_score + 1;
--						end if;
--
--						--Next level logic
--						case current_score is
--							when 0 to LEVEL_2_THRESHHOLD - 1 =>
--								current_level := level1;
--							when LEVEL_2_THRESHHOLD to LEVEL_3_THRESHHOLD - 1 =>
--								current_level := level2;
--							when LEVEL_3_THRESHHOLD to current_score'high =>
--								current_level := level3;
--						end case;
--					end loop;
--
--					case current_level is
--						when level1 =>
--							level <= 1;
--						when level2 =>
--							level <= 2;
--						when level3 =>
--							level <= 3;
--					end case;					
--
--					--Perform top and bottom bounds checking
--					--Check for collisions
--					if ( bird_collides_with_screen(v_bird_y) ) then
--						----Check for top and bottom collisions
--						current_playing_state <= dead;
--					end if;
--
--					for i in pipes_x'range loop
--						--Check for bullets
--						if item_collides_with_pipe(pipes_x(i), pipes_y(i), v_bullet_x, v_bullet_y, bullet_size, bullet_size) or v_bullet_x + bullet_size >= HORIZONTAL_RESOLUTION then
--							v_bullet_state := bullet_state_idle;
--							v_bullet_x := -1;
--							v_bullet_y := -1;
--						end if;
--
--						if item_collides_with_pipe(pipes_x(i), pipes_y(i), v_bird_x, v_bird_y, BIRD_HORIZONTAL_SIZE, BIRD_VERTICAL_SIZE) then
--							--well shet
--							current_playing_state <= dead;
--						end if;
--
--						if v_flowers(i) = '1' then
--							--If Bullet collides with flower
--							if ( (pipes_x(i) + 5 < v_bullet_x + bullet_size) and (pipes_x(i) + 45 > v_bullet_x) and (pipes_y(i) + 30 < v_bullet_y + bullet_size) and (pipes_y(i) + 120 > v_bullet_y) ) then
--								v_bullet_state := bullet_state_idle;
--								v_bullet_x := -1;
--								v_bullet_y := -1;
--								v_flowers(i) := '0';
--							end if;
--
--							--If bird collides with flower
--							if ( (pipes_x(i) + 5 < v_bird_x + BIRD_HORIZONTAL_SIZE) and (pipes_x(i) + 45 > v_bird_x) and (pipes_y(i) + 30 < v_bird_y + BIRD_VERTICAL_SIZE) and (pipes_y(i) + 120 > v_bird_y) ) then
--								current_playing_state <= dead;
--							end if;
--						end if;
--					end loop;


				-------------------------------------------------------------------------
				--						Training State								   --
				-------------------------------------------------------------------------
--				when training =>
--					if(flap = '1') then
--						velocity := flap_speed;
--					else
--						velocity := velocity + GRAVITY;
--						if(velocity > MAX_FALLING_SPEED) then
--							velocity := MAX_FALLING_SPEED;
--						end if;
--					end if;
--					v_bird_y := v_bird_y + velocity;
--
--					if(v_bird_y < 0) then
--						v_bird_y := 0;
--					end if;
--
--					--Pipe speed is constant in training mode
--					current_pipe_speed := PIPE_SPEED_NORMAL;								
--
--					--Move pipes
--					for i in pipes_x'range loop
--						pipes_x(i) := pipes_x(i) - current_pipe_speed;
--
--						--if pipe is off screen
--						--move pipe to other side
--						if(pipes_x(i) + PIPE_WIDTH < 0) then
--							pipes_x(i) := pipes_x( (i+2) mod pipes_x'length) + 400 ;
--							pipes_y(i) := TO_INTEGER(UNSIGNED(generated_pipe_location));
--							if(current_level = level3) then
--								v_flowers(i) := '1';
--							end if;
--						end if;
--
--						if item_collides_with_pipe(pipes_x(i), pipes_y(i), v_bird_x, v_bird_y, BIRD_HORIZONTAL_SIZE, BIRD_VERTICAL_SIZE) then
--							--well shet
--							current_playing_state <= dead;
--						end if;
--
--						--TODO - need a more reliable way to detect score
--						if ((v_bird_x >= pipes_x(i)) AND (v_bird_x < (pipes_x(i) + current_pipe_speed))) then
--							current_score := current_score + 1;
--						end if;		
--
--					--Perform top and bottom bounds checking
--					--Check for collisions
--						if ( bird_collides_with_screen(v_bird_y) ) then
--						----Check for top and bottom collisions
--							current_playing_state <= dead;
--						end if;
--
--					--PIPE
--						if item_collides_with_pipe(pipes_x(i), pipes_y(i), v_bird_x, v_bird_y, BIRD_HORIZONTAL_SIZE, BIRD_VERTICAL_SIZE) then
--						--well shet
--							current_playing_state <= dead;
--						end if;
--
--						level <= 0;
--					end loop;


				-------------------------------------------------------------------------
				--						Dead State									   --
				-------------------------------------------------------------------------
				when dead =>
					--Well shet
					--Bird stand still for a second
					if(death_standstill = '1') then
						death_counter := death_counter + 1;
						if(death_counter > 50) then 
							death_counter := 0;
							death_standstill := '0';
						end if;
					else
						v_bird_y := v_bird_y + MAX_FALLING_SPEED;
						if(v_bird_y > VERTICAL_RESOLUTION) then
							v_bird_y := VERTICAL_RESOLUTION;
							death_counter := death_counter + 1;
							if(death_counter > 60) then
							--Bird starts falling after a second
								current_playing_state <= reset;
							end if;
						end if;

					end if;
			end case;
		end if;
		--outputs
		score <= current_score;

		bird_x_pos <= v_bird_x;
		bird_y_pos <= v_bird_y;

		pipe_A_x <= pipes_x(0);
		pipe_A_y <= pipes_y(0);

		pipe_B_x <= pipes_x(1);
		pipe_B_y <= pipes_y(1);

		pipe_C_x <= pipes_x(2);
		pipe_C_y <= pipes_y(2);

		current_bullet_state <= v_bullet_state;	
		bullet_x <= v_bullet_x;
		bullet_y <= v_bullet_y;

		--NOTE - If the enumerated type flappy_fsm ever changes, this code is likely to break
		case current_playing_state is
			when reset =>
				game_state <= FLAPPY_FSM_RESET;
			when menu =>
				game_state <= FLAPPY_FSM_MENU;
			--when idle =>
			--	game_state <= FLAPPY_FSM_IDLE;
			when play =>
				game_state <= FLAPPY_FSM_PLAY;
			when training =>
				game_state <= FLAPPY_FSM_TRAINING;
			when dead =>
				game_state <= FLAPPY_FSM_DEAD;
		end case;

		flowers <= v_flowers;
	end process world_step;

end architecture pipes;
