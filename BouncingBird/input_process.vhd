library ieee;
use ieee.std_logic_1164.all;

entity click_gen is
	port( mouse_left_click: in std_logic;
			clk_50Hz: in std_logic;
			flap: out std_logic
	);
end entity click_gen;

architecture derp of click_gen is
	type button_fsm is (idle, button_pushed, button_held);
	signal click_state: button_fsm := idle;
begin
	generate_flap: process(clk_50Hz)
	begin
		if(rising_edge(clk_50Hz)) then
			case click_state is
				when idle =>
					if (mouse_left_click = '1') then
						click_state <= button_pushed;
					end if;
				when button_pushed =>
					click_state <= button_held;
				when button_held =>
					if (mouse_left_click = '0') then
						click_state <= idle;
					end if;
			end case;
		end if;
	end process;

	process(click_state)
	begin
		case click_state is
			when idle =>
				flap <= '0';
			when button_pushed =>
				flap <= '1';
			when button_held =>
				flap <= '0';
		end case;
	end process;
end architecture derp;