LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library flappy_types;
use work.flappy_types.all;

entity message_generator is
	port ( 	frame_row, frame_col, current_row, current_col : in integer range 0 to 1024;
			clk, enable : in std_logic;
			character_array : in char_array;
			font_out : out std_logic
	);
end entity message_generator;

architecture the_road_not_taken of message_generator is

	component char_rom is	
		port ( character_address	:	IN STD_LOGIC_VECTOR (5 DOWNTO 0);
			   font_row, font_col	:	IN STD_LOGIC_VECTOR (2 DOWNTO 0);
			   clock				: 	IN STD_LOGIC ;
			   rom_mux_output		:	OUT STD_LOGIC
		);
	end component;
	
	signal row, col : std_logic_vector (2 downto 0);
	signal rom_out : std_logic;
	signal current_char : std_logic_vector(5 downto 0);
	
begin
	character_rom : char_rom port map (current_char, row, col, clk, rom_out);
					
	process (clk)
		variable v_row, v_col : integer range 0 to 1024;	
	begin
		if ((current_row >= frame_row) AND (current_col >= frame_col) AND (current_row < frame_row + MESSAGE_HEIGHT) AND (current_col < frame_col + MESSAGE_WIDTH)) then
			v_row := current_row - frame_row;
			v_col := current_col - frame_col;
			case v_col is
				when 0 to (CHARACTER_WIDTH -1) =>
					current_char <= character_array(0);
				when CHARACTER_WIDTH to (2 * CHARACTER_WIDTH -1) =>
					current_char <= character_array(1);
				when 2 * CHARACTER_WIDTH to (3 * CHARACTER_WIDTH -1) =>
					current_char <= character_array(2);
				when 3* CHARACTER_WIDTH to (4* CHARACTER_WIDTH -1) =>
					current_char <= character_array(3);
				when 4* CHARACTER_WIDTH to (5* CHARACTER_WIDTH -1) =>
					current_char <= character_array(4);
				when 5* CHARACTER_WIDTH to (6* CHARACTER_WIDTH -1) =>
					current_char <= character_array(5);
				when 6* CHARACTER_WIDTH to (7* CHARACTER_WIDTH -1) =>
					current_char <= character_array(6);
				when 7* CHARACTER_WIDTH to (8* CHARACTER_WIDTH -1) =>
					current_char <= character_array(7);
				when 8* CHARACTER_WIDTH to (9* CHARACTER_WIDTH -1) =>
					current_char <= character_array(8);
				when others =>
					current_char <= O"40";
			end case;									
		else 
			v_row := 0;
			v_col := 0;
			current_char <= O"01";
		end if;
		col <= std_logic_vector(to_unsigned(v_col, 3));
		row <= std_logic_vector(to_unsigned(v_row, 3));
	end process;

	with enable select
		font_out <= rom_out when '1',
							'0' when others;
	
end architecture;
