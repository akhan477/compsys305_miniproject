LIBRARY IEEE;
USE IEEE.std_logic_1164.all;

ENTITY clk_divider IS
  PORT (clk: IN STD_LOGIC;
        clk_25_MHz: OUT STD_LOGIC);
END ENTITY;

ARCHITECTURE behavioural OF clk_divider IS
BEGIN
  
  PROCESS (clk)
    VARIABLE count: INTEGER range 0 TO 100000000;
    VARIABLE temp: STD_LOGIC := '0';
    
  BEGIN
    IF RISING_EDGE(clk) THEN
      	temp := NOT temp;	
		clk_25_MHz <= temp;
	END IF;
  END PROCESS;
  
END ARCHITECTURE behavioural;
