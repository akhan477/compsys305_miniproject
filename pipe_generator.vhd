LIBRARY IEEE;
USE  IEEE.STD_LOGIC_1164.all;

ENTITY pipe_generator IS 
	PORT ( clk : IN std_logic;
				 reset : IN std_logic;
				 p_top : OUT std_logic_vector(8 DOWNTO 0)
			);
END ENTITY pipe_generator;

ARCHITECTURE structure OF pipe_generator IS 

	COMPONENT lfsr IS
		PORT ( clk : IN std_logic;
			 reset : IN std_logic;
			 num_out : OUT std_logic_vector(4 DOWNTO 0)
		);
	END COMPONENT;
	
	COMPONENT pipe_rom IS
	PORT
	(
		address				:	IN STD_LOGIC_VECTOR (4 DOWNTO 0);
		clock				: 	IN STD_LOGIC ;
		q					: 	OUT STD_LOGIC_VECTOR (8 DOWNTO 0)
	);
	END COMPONENT;
	
	SIGNAL temp_address : STD_LOGIC_VECTOR(4 DOWNTO 0);
	
BEGIN
	rand_gen: lfsr PORT MAP (clk, reset, temp_address);
	rom: pipe_rom PORT MAP (temp_address, clk, p_top);
END ARCHITECTURE structure;