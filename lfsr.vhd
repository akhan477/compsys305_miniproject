LIBRARY IEEE;
USE  IEEE.STD_LOGIC_1164.all;

ENTITY lfsr IS
	PORT ( clk : IN std_logic;
				 reset: IN std_logic;
				 num_out : OUT std_logic_vector(4 DOWNTO 0)
				 );
END ENTITY lfsr;

ARCHITECTURE behaviour OF lfsr IS
BEGIN

		PROCESS (clk, reset)
			VARIABLE rnum: std_logic_vector(4 DOWNTO 0) := "10101";
		BEGIN
			IF reset = '1' THEN 
				rnum := "10101";
			ELSIF rising_edge(clk) THEN
				rnum := rnum(0) & rnum(4) & (rnum(3) xor rnum(0)) & rnum(2 DOWNTO 1);
			END IF;
			
			num_out <= rnum;
			
		END PROCESS;
	END ARCHITECTURE behaviour;
	